#!/bin/sh

set -eu

HERE="$(dirname "$(realpath "$0")")"

sudo mkdir -p /usr/local/etc/keyszer/
sudo cp "$HERE/config.py" /usr/local/etc/keyszer/config.py
