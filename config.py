# keyszer(xkeysnail fork) config file for additional keyboard layers
# https://github.com/joshgoebel/keyszer

# suspend - The number of seconds modifiers are "suspended" and
# withheld from the output waiting to see whether if they are part
# of a combo or if they may be the actual intended output.
# multipurpose - The number of seconds before a multipurpose keymap
# starts to act as a held modifier instead of waiting for release
# to act as a pressed key
timeouts(
    #multipurpose = 0.2,
    suspend = 0,
)

# add a new modifier key for capslock to re-identify as
Modifier("Helvetin Vittu", aliases = ["HV"], key = Key.F20)
HV = Key.F20

modmap("capslock as F20 modifier",
    {Key.CAPSLOCK: HV}
)

# [Global Keymap] Change how keys keys are registered
keymap("Saatanan Perkele", {
    # mostly QWERTY left to right row by row order
    # nordic keyboard layout btw

    # left hand's keys
    C("HV-q"): C("scrolllock"),
    C("HV-w"): C("home"),
    C("HV-e"): C("up"),
    C("HV-r"): C("end"),
    C("HV-t"): C("page_up"),
    C("HV-a"): C("esc"),
    C("HV-s"): C("left"),
    C("HV-d"): C("down"),
    C("HV-f"): C("right"),
    C("HV-g"): C("page_down"),

    # {{{ Arrows & home/end with modifiers
    C("HV-LAlt-q"): C("Shift-LAlt-up"),
    C("HV-LAlt-a"): C("Shift-LAlt-down"),

    C("HV-Shift-w"): C("Shift-home"),
    C("HV-Shift-e"): C("Shift-up"),
    C("HV-Shift-r"): C("Shift-end"),
    C("HV-Shift-s"): C("Shift-left"),
    C("HV-Shift-d"): C("Shift-down"),
    C("HV-Shift-f"): C("Shift-right"),

    C("HV-C-w"): C("C-home"),
    C("HV-C-e"): C("C-up"),
    C("HV-C-r"): C("C-end"),
    C("HV-C-s"): C("C-left"),
    C("HV-C-d"): C("C-down"),
    C("HV-C-f"): C("C-right"),

    C("HV-LAlt-w"): C("LAlt-home"),
    C("HV-LAlt-e"): C("LAlt-up"),
    C("HV-LAlt-r"): C("LAlt-end"),
    C("HV-LAlt-s"): C("LAlt-left"),
    C("HV-LAlt-d"): C("LAlt-down"),
    C("HV-LAlt-f"): C("LAlt-right"),

    C("HV-Shift-C-w"): C("Shift-C-home"),
    C("HV-Shift-C-e"): C("Shift-C-up"),
    C("HV-Shift-C-r"): C("Shift-C-end"),
    C("HV-Shift-C-s"): C("Shift-C-left"),
    C("HV-Shift-C-d"): C("Shift-C-down"),
    C("HV-Shift-C-f"): C("Shift-C-right"),

    C("HV-Shift-LAlt-w"): C("Shift-LAlt-home"),
    C("HV-Shift-LAlt-e"): C("Shift-LAlt-up"),
    C("HV-Shift-LAlt-r"): C("Shift-LAlt-end"),
    C("HV-Shift-LAlt-s"): C("Shift-LAlt-left"),
    C("HV-Shift-LAlt-d"): C("Shift-LAlt-down"),
    C("HV-Shift-LAlt-f"): C("Shift-LAlt-right"),

    C("HV-Shift-C-LAlt-w"): C("Shift-C-LAlt-home"),
    C("HV-Shift-C-LAlt-e"): C("Shift-C-LAlt-up"),
    C("HV-Shift-C-LAlt-r"): C("Shift-C-LAlt-end"),
    C("HV-Shift-C-LAlt-s"): C("Shift-C-LAlt-left"),
    C("HV-Shift-C-LAlt-d"): C("Shift-C-LAlt-down"),
    C("HV-Shift-C-LAlt-f"): C("Shift-C-LAlt-right"),

    C("HV-C-LAlt-w"): C("Shift-C-LAlt-home"),
    C("HV-C-LAlt-e"): C("Shift-C-LAlt-up"),
    C("HV-C-LAlt-r"): C("Shift-C-LAlt-end"),
    C("HV-C-LAlt-s"): C("Shift-C-LAlt-left"),
    C("HV-C-LAlt-d"): C("Shift-C-LAlt-down"),
    C("HV-C-LAlt-f"): C("Shift-C-LAlt-right"),
    # }}}

    # unihand keys
    C("HV-space"):            C("enter"),
    C("HV-Shift-space"):      C("Shift-enter"),

    # right hand's keys
    C("HV-LAlt-RAlt-u"): C("playpause"),
    C("HV-LAlt-RAlt-i"): C("previoussong"),
    C("HV-LAlt-RAlt-o"): C("nextsong"),

    C("HV-h"):           C("backspace"),
    C("HV-semicolon"):   C("delete"),     # HV-ö on scandinavian kbs
    C("HV-C-h"):         C("C-backspace"),
    C("HV-C-semicolon"): C("C-delete"),   # HV-C-ö on scandinavian kbs

    # numbers in numpad format for right hand
    C("HV-u"): C("KEY_7"), C("HV-i"):     C("KEY_8"), C("HV-o"):   C("KEY_9"),
    C("HV-j"): C("KEY_4"), C("HV-k"):     C("KEY_5"), C("HV-l"):   C("KEY_6"),
    C("HV-m"): C("KEY_1"), C("HV-comma"): C("KEY_2"), C("HV-dot"): C("KEY_3"),
    C("HV-n"): C("KEY_0"), C("HV-slash"): C("minus"), # <- means - is + with caps mod

    # Left Alt + num, numbers in numpad format for right hand
    C("HV-LAlt-u"): C("Super-KEY_7"), C("HV-LAlt-i"):     C("Super-KEY_8"), C("HV-LAlt-o"):   C("Super-KEY_9"),
    C("HV-LAlt-j"): C("Super-KEY_4"), C("HV-LAlt-k"):     C("Super-KEY_5"), C("HV-LAlt-l"):   C("Super-KEY_6"),
    C("HV-LAlt-m"): C("Super-KEY_1"), C("HV-LAlt-comma"): C("Super-KEY_2"), C("HV-LAlt-dot"): C("Super-KEY_3"),
    C("HV-LAlt-n"): C("Super-KEY_0"),

    # Shift + Left Alt + num, numbers in numpad format for right hand
    C("HV-Shift-LAlt-u"): C("Super-Shift-KEY_7"), C("HV-Shift-LAlt-i"):     C("Super-Shift-KEY_8"), C("HV-Shift-LAlt-o"):   C("Super-Shift-KEY_9"),
    C("HV-Shift-LAlt-j"): C("Super-Shift-KEY_4"), C("HV-Shift-LAlt-k"):     C("Super-Shift-KEY_5"), C("HV-Shift-LAlt-l"):   C("Super-Shift-KEY_6"),
    C("HV-Shift-LAlt-m"): C("Super-Shift-KEY_1"), C("HV-Shift-LAlt-comma"): C("Super-Shift-KEY_2"), C("HV-Shift-LAlt-dot"): C("Super-Shift-KEY_3"),
    C("HV-Shift-LAlt-n"): C("Super-Shift-KEY_0"),

    # Xorg pointer keys
    C("HV-z"): C("Shift-NUMLOCK"), # on-switch
    C("HV-Shift-u"): C("KP7"), C("HV-Shift-i"):     C("KP8"), C("HV-Shift-o"):   C("KP9"),
    C("HV-Shift-j"): C("KP4"), C("HV-Shift-k"):     C("KP5"), C("HV-Shift-l"):   C("KP6"),
    C("HV-Shift-m"): C("KP1"), C("HV-Shift-comma"): C("KP2"), C("HV-Shift-dot"): C("KP3"),
   #C("HV-Shift-k"):         C("BTN_LEFT"),   # Left   Mouse Button
    C("HV-Shift-h"):         C("BTN_MIDDLE"), # Middle Mouse Button
    C("HV-Shift-semicolon"): C("BTN_RIGHT"),  # Right  Mouse Button
    C("HV-Shift-n"): C("KP0"), # click and hold until 5 is pressed

    # special symbols with HV and right alt

    # wer ty uio
    # {[( ~* ]]}

    C("HV-RAlt-w"):          C("RAlt-KEY_7"),       # {
    C("HV-RAlt-e"):          C("RAlt-KEY_8"),       # [
    C("HV-RAlt-r"):          C("Shift-KEY_8"),      # (

    C("HV-RAlt-t"):          C("RAlt-right_brace"), # ~
    C("HV-RAlt-y"):          C("Shift-backslash"),  # *

    C("HV-RAlt-u"):          C("Shift-KEY_9"),      # )
    C("HV-RAlt-i"):          C("RAlt-KEY_9"),       # ]
    C("HV-RAlt-o"):          C("RAlt-KEY_0"),       # }

    # asdfg hjklöä
    # =!'?/ \#"$%`

    C("HV-RAlt-a"):          C("Shift-KEY_0"),      # =
    C("HV-RAlt-s"):          C("Shift-KEY_1"),      # !
    C("HV-RAlt-d"):          C("backslash"),        # '
    C("HV-RAlt-f"):          C("Shift-minus"),      # ?
    C("HV-RAlt-g"):          C("Shift-KEY_7"),      # /

    C("HV-RAlt-h"):          C("RAlt-minus"),       # \
    C("HV-RAlt-j"):          C("Shift-KEY_3"),      # #
    C("HV-RAlt-k"):          C("Shift-KEY_2"),      # "
    C("HV-RAlt-l"):          C("RAlt-KEY_4"),       # $
    C("HV-RAlt-semicolon"):  C("Shift-KEY_5"),      # %
    C("HV-RAlt-apostrophe"): C("Shift-equal"),      # `

    # zxcvb nm,.-
    # |&@<| ^>

    C("HV-RAlt-z"):     C("RAlt-key_102ND"),    # |
    C("HV-RAlt-x"):     C("Shift-KEY_6"),       # &
    C("HV-RAlt-c"):     C("RAlt-KEY_2"),        # @
    C("HV-RAlt-v"):     C("KEY_102ND"),         # <
    C("HV-RAlt-b"):     C("RAlt-key_102ND"),    # |
    C("HV-RAlt-n"):     C("Shift-right_brace"), # ^
    C("HV-RAlt-m"):     C("Shift-KEY_102ND"),   # >
#   C("HV-RAlt-comma"): C(""),
#   C("HV-RAlt-dot"):   C(""),
#   C("HV-RAlt-slash"): C(""),
})
